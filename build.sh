#!/bin/sh
echo Building github-listener:build

docker build -t github-listener:build . -f Dockerfile.build

docker container create --name extract github-listener:build  
docker container cp extract:/go/src/gitlab.com/hoplite/sidekick/github-listener/main ./app
docker container rm -f extract

echo Building github-listener:latest

docker build --no-cache -t github-listener:latest .
rm ./app
