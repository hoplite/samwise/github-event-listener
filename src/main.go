package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/google/go-cmp/cmp"
	"github.com/gorilla/mux"
)

type Webhook struct {
	Action      string      `json: "action"`
	Type        string      `json:"type"`
	Ref         string      `json:"ref"`
	Created     bool        `json:"created"`
	Repo        *Repository `json:"repository"`
	Pusher      *Pusher     `json:"pusher,omitempty"`
	Commits     []Commit    `json:"commits,omitempty"`
	HeadCommit  Commit      `json:"head_commit,omitempty"`
	PullRequest PullRequest `json:"pull_request,omitempty"`
	Sender      *User       `json:"sender, omitempty"`
}

type Repository struct {
	Url      string `json:"url,omitempty"`
	Name     string `json:"name,omitempty"`
	FullName string `json:"full_name,omitempty"`
}

type Branch struct {
	Ref string `json:"ref"`
}

type Commit struct {
	Id      string  `json:"id"`
	Message string  `json:"message"`
	Author  *Author `json:"author"`
	Url     string  `json:"url"`
}

type Author struct {
	Name     string `json:"name,omitempty"`
	Username string `json:"username,omitempty"`
	Email    string `json:"email,omitempty"`
}

type Pusher struct {
	Name  string `json:"name,omitempty"`
	Email string `json:"email,omitempty"`
}

type User struct {
	Login string `json:"login,omitempty"`
}

type PullRequest struct {
	Body       string  `json:"body, omitempty"`
	BaseBranch *Branch `json:"base"`
	HeadBranch *Branch `json:"head"`
	Title      string  `json:"title,omitempty"`
	Url        string  `json:"html_url,omitempty"`
}

type Message struct {
	Service     string
	RoomID      string
	MessageText string
}

func CreateMessage(service string, webhook *Webhook) Message {
	var b bytes.Buffer
	var message Message
	var committer string
	var MessageText string

	message.Service = service
	numCommits := len(webhook.Commits)
	repoName := webhook.Repo.FullName
	branchRef := webhook.Ref

	if cmp.Equal(webhook.HeadCommit, Commit{}) == false {
		headCommit := webhook.HeadCommit
		committer = headCommit.Author.Name
	} else if cmp.Equal(webhook.Sender, User{}) == false {
		committer = webhook.Sender.Login
	} else {
		committer = webhook.Pusher.Name
	}

	if webhook.Created {
		MessageText = fmt.Sprintf("%s created %s on %s ", committer, branchRef, repoName)
	} else if cmp.Equal(webhook.PullRequest, PullRequest{}) == false {
		pr := webhook.PullRequest
		MessageText = fmt.Sprintf("%s %s pull request %s on %s\n %s\n",
			committer, webhook.Action, pr.Title, repoName, pr.Url)
	} else {
		MessageText = fmt.Sprintf("%s pushed to %s on %s ", committer, branchRef, repoName)
	}

	b.WriteString(MessageText)

	if numCommits > 0 {
		b.WriteString("with " + strconv.Itoa(numCommits))
		if numCommits > 1 {
			b.WriteString(" commits\n")
		} else {
			b.WriteString(" commit\n ")
		}

		for _, commit := range webhook.Commits {
			b.WriteString(commit.Message + " " + commit.Url + "\n")
		}
	}
	message.MessageText = b.String()

	return message
}

func GetWebhook(w http.ResponseWriter, r *http.Request) {
	var webhook Webhook
	RelayUrl := fmt.Sprintf("%s/messages/", os.Getenv("MSG_RELAY_URL"))
	log.Println("Relay url is", RelayUrl)
	log.Printf("Request on URL: %s from host %s\n", r.URL, r.RemoteAddr)
	log.Println("Req", RelayUrl)
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))

	if err != nil {
		panic(err)
	}

	if err := r.Body.Close(); err != nil {
		panic(err)
	}

	if err := json.Unmarshal(body, &webhook); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessablle entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	if cmp.Equal(webhook, Webhook{}) {
		err := "JSON is incorrect"
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
		log.Println("Encounters error", err)
		return
	}

	params := mux.Vars(r)
	log.Printf("params: %+v\n", params)
	log.Println("RoomID: ", params["roomID"])
	log.Printf("Webhook: %+v\n", webhook)
	log.Println("Webhook Pusher: ", webhook.Pusher)
	log.Println("Head commit: ", webhook.HeadCommit)
	message := CreateMessage(params["service"], &webhook)
	message.RoomID = params["roomID"]
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(message)
	res, err := http.Post(RelayUrl,
		"application/json; charset=utf-8", b)
	if err != nil {
		log.Fatal(err)
	}

	io.Copy(os.Stdout, res.Body)
}

func main() {
	router := mux.NewRouter()
	log.Printf("Starting service")
	router.HandleFunc("/events/{service}/{roomID}", GetWebhook).Methods("POST")
	log.Fatal(http.ListenAndServe(":8000", router))
}
